GO ?= go

PROTO_PATH := ./vendor

generate:
	@protoc -I services -I ${PROTO_PATH} --gogo_out=plugins=grpc:services services/*.proto
	@$(GO) fmt ./...
.PHONY: generate

build: generate
	@echo "====> Build gen"
	@$(GO) build -o ./bin/rpc main.go
.PHONY: build

install.dev:
	@$(GO) get github.com/golang/dep/cmd/dep
	@dep ensure
.PHONY: install.dev

release:
	@goreleaser --skip-publish --skip-validate --rm-dist --snapshot
.PHONY: release
