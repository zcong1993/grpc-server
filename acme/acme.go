package acme

import (
	"crypto/tls"
	"golang.org/x/crypto/acme/autocert"
)

func GetTLS(host, cacheDir, contactEmail string) (*tls.Config, error) {
	manager := autocert.Manager{
		Prompt:     autocert.AcceptTOS,
		Cache:      autocert.DirCache(cacheDir),
		HostPolicy: autocert.HostWhitelist(host),
		Email:      contactEmail,
	}
	return &tls.Config{GetCertificate: manager.GetCertificate}, nil
}
